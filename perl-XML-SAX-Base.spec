%define mod_name XML-SAX-Base

Name:           perl-XML-SAX-Base
Version:        1.09
Release:        9
Summary:        Base class SAX Drivers and Filters
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/G/GR/GRANTM/%{mod_name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils, perl-interpreter, perl-generators
BuildRequires:  perl(base), perl(Carp), perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(overload), perl(strict), perl(Test), perl(Test::More) >= 0.88
BuildRequires:  perl(vars), perl(warnings)
Conflicts:      perl-XML-SAX < 0.99-1

%description
This module is a base class for Perl SAX drivers and filters. The default behavior is
to pass the input directly to the output without any changes. This module can be used
as a base class so that the user don't have to implement the characters() callback.

%package        help
Summary:        man files for perl-XML-SAX-Base

%description    help
This package includes man files for perl-XML-SAX-Base.

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT

install -d $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
mv $RPM_BUILD_ROOT%{perl_vendorlib}/XML/SAX/BuildSAXBase.pl $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}

%check
make test

%files
%doc README
%{perl_vendorlib}/*
%{_docdir}/%{name}-%{version}/*

%files help
%{_mandir}/man*/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.09-9
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Oct 31 2022 hongjinghao <hongjinghao@huawei.com> - 1.09-8
- use %{mod_name} marco

* Wed Nov 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.09-7
- Package init
